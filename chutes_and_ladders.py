#!/usr/bin/python
import random
import time

game = 0
while True:
    game += 1
    player = 0
    steps = 0
    big_slide = 0

    start = time.time()

    while True:
        roll = random.randint(1,6)
        steps += 1
        if(player + roll > 100):
            #print "Stuck at", player
            continue
        player += roll
        #print "Rolled a", roll, "player at", player
        #ladders
        if(player == 1):
            #print "#### ladder"
            player = 38
        elif (player == 4):
            #print "#### ladder"
            player = 14
        elif (player == 9):
            #print "#### ladder"
            player = 31
        elif (player == 28):
            #print "#### ladder"
            player = 84
        elif (player == 21):
            #print "#### ladder"
            player = 42
        elif (player == 36):
            #print "#### ladder"
            player = 44
        elif (player == 51):
            #print "#### ladder"
            player = 67
        elif (player == 71):
            #print "#### ladder"
            player = 91
        elif (player == 80):
            #print "#### ladder"
            player = 100
        #chutes
        if(player == 98):
            #print "**** chute"
            player = 78
        elif (player == 93):
            #print "**** chute"
            player = 73
        elif (player == 95):
            #print "**** chute"
            player = 75
        elif (player == 87):
            #print "**** chute"
            big_slide += 1
            player = 25
        elif (player == 64):
            #print "**** chute"
            player = 60
        elif (player == 62):
            #print "**** chute"
            player = 19
        elif (player == 56):
            #print "**** chute"
            player = 53
        elif (player == 48):
            #print "**** chute"
            player = 26
        elif (player == 49):
            #print "**** chute"
            player = 11
        elif (player == 16):
            #print "**** chute"
            player = 6

        #print "Player now at",player

        if(player == 100):
            #print "won the game!"
            break
        if(steps > 2000000):
            break

    total = time.time() - start
    print game,",",steps,",",big_slide,",",int(total*1000000)
    if(game > 2000000):
        break
    #if(steps < 7 or steps > 378):
    #    print game,",",steps,",",int(total*1000000)
